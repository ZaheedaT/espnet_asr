# espnet_asr

ESPnet is an end-to-end speech processing toolkit, mainly focuses on end-to-end speech recognition and end-to-end text-to-speech. ESPnet uses chainer and pytorch as a main deep learning engine, and also follows Kaldi style data processing.

# Setup
```sh
$ sudo apt-get install bc tree
$ cat /etc/os-release
```
# espnet setup
```sh
$ git clone https://github.com/espnet/espnet
$ cd espnet; pip install -e .
$ mkdir -p espnet/tools/venv/bin; touch espnet/tools/venv/bin/activate
```
# warp ctc setup
```sh
$ git clone https://github.com/espnet/warp-ctc -b pytorch-1.1
$ cd warp-ctc && mkdir build && cd build && cmake .. && make -j4
$ cd warp-ctc/pytorch_binding && python setup.py install
```
# kaldi setup
```sh
$ cd ./espnet/tools; git clone https://github.com/kaldi-asr/kaldi
$ echo "" > ./espnet/tools/kaldi/tools/extras/check_dependencies.sh # ignore check
$ chmod +x ./espnet/tools/kaldi/tools/extras/check_dependencies.sh
$ cd ./espnet/tools/kaldi/tools; make sph2pipe sclite
$ rm -rf espnet/tools/kaldi/tools/python
$ [ ! -e ubuntu16-featbin.tar.gz ] && wget https://18-198329952-gh.circle-artifacts.com/0/home/circleci/repo/ubuntu16-featbin.tar.gz
$ tar -xf ./ubuntu16-featbin.tar.gz
$ cp featbin/* espnet/tools/kaldi/src/featbin/
```
## Edit the data paths open
Open and edit the path.sh file
specify where the data should be put or where the data should be read if it exists

```sh
$ cd ~/espnet/egs/librispeech/asr1
$ nano path.sh
$ datadir=path/to/data
```

## Train the model

```sh
$ cd ~/espnet/egs/librispeech/asr1
$ ./run.sh 
```

## Training log
```
2019-08-30 13:05:31,594 (lm:207) INFO: #vocab = 5001
2019-08-30 13:05:31,594 (lm:208) INFO: #sentences in the training data = 40629122
2019-08-30 13:05:31,594 (lm:209) INFO: #tokens in the training data = 1021433253
2019-08-30 13:05:31,594 (lm:210) INFO: oov rate in the training data = 0.00 %
2019-08-30 13:05:31,594 (lm:211) INFO: #sentences in the validation data = 5542
2019-08-30 13:05:31,594 (lm:212) INFO: #tokens in the validation data = 128834
2019-08-30 13:05:31,594 (lm:213) INFO: oov rate in the validation data = 0.00 %
2019-08-30 13:06:08,992 (lm:224) INFO: #iterations per epoch = 95687
2019-08-30 13:06:08,992 (lm:225) INFO: #total iterations = 1913740
2019-08-30 13:06:31,366 (lm:241) INFO: writing a model config file to exp/train_rnnlm_pytorch_lm_unigram5000/model.json
epoch       iteration   main/loss   perplexity  val_perplexity  elapsed_time
[J0           100         286.299     5507.42                     538.365       
[J     total [..................................................]  0.01%
this epoch [..................................................]  0.10%
       100 iter, 0 epoch / 20 epochs
       inf iters/sec. Estimated time to finish: 0:00:00.
[4A[J0           200         208.574     996.853                     1059.75       
[J     total [..................................................]  0.01%
this epoch [..................................................]  0.21%
       200 iter, 0 epoch / 20 epochs
   0.19181 iters/sec. Estimated time to finish: 115 days, 11:10:37.854658.
[4A[J0           300         222.129     896.064                     1632.5        
[J     total [..................................................]  0.02%
this epoch [..................................................]  0.31%
       300 iter, 0 epoch / 20 epochs
    0.1828 iters/sec. Estimated time to finish: 121 days, 3:37:11.452918.
[4A[J0           400         222.916     871.27                      2196.6        
[J     total [..................................................]  0.02%
this epoch [..................................................]  0.42%
       400 iter, 0 epoch / 20 epochs
   0.18092 iters/sec. Estimated time to finish: 122 days, 9:40:20.855591.
[4A[J0           500         260.297     1009.83                     2791.99       
[J     total [..................................................]  0.03%
this epoch [..................................................]  0.52%
       500 iter, 0 epoch / 20 epochs
   0.17749 iters/sec. Estimated time to finish: 124 days, 18:12:35.989888.
   
   ...
   ```