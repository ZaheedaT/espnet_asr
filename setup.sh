#!/bin/bash
# a bash script to setup the environment for the project

# install pip3 and virtual
sudo apt-get install python3-pip -y

# create the virutual environment in the project root
pip3 install virtualenv

# activate the project environment and install the packages
virtualenv --no-site-packages -p python3 espnet_env
source espnet_env/bin/activate
pip install -r requirements.txt

# create the ipython kernel for the tutorials

pip install ipykernel
python -m ipykernel install --user --name espnetkernel --display-name "Espnet Kernel"